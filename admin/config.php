<?php
// HTTP
define('HTTP_SERVER', 'http://sattv/admin/');
define('HTTP_CATALOG', 'http://sattv/');

// HTTPS
define('HTTPS_SERVER', 'http://sattv/admin/');
define('HTTPS_CATALOG', 'http://sattv/');

// DIR
define('DIR_APPLICATION', 'C:/OSPanel/domains/sattv/admin/');
define('DIR_SYSTEM', 'C:/OSPanel/domains/sattv/system/');
define('DIR_IMAGE', 'C:/OSPanel/domains/sattv/image/');
define('DIR_LANGUAGE', 'C:/OSPanel/domains/sattv/admin/language/');
define('DIR_TEMPLATE', 'C:/OSPanel/domains/sattv/admin/view/template/');
define('DIR_CONFIG', 'C:/OSPanel/domains/sattv/system/config/');
define('DIR_CACHE', 'C:/OSPanel/domains/sattv/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OSPanel/domains/sattv/system/storage/download/');
define('DIR_LOGS', 'C:/OSPanel/domains/sattv/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OSPanel/domains/sattv/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OSPanel/domains/sattv/system/storage/upload/');
define('DIR_CATALOG', 'C:/OSPanel/domains/sattv/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'sattv');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
