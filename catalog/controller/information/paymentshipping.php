<?php

class ControllerInformationPaymentshipping extends Controller
{

    private $error = array();

    public function index()
    {
        $this->load->language('information/paymentshipping');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['details_1'] = $this->language->get('details_1');
        $data['details_2'] = $this->language->get('details_2');
        $data['details_3'] = $this->language->get('details_3');
        $data['details_4'] = $this->language->get('details_4');


        //shipping methods
        $data['shipping_methods_title'] = $this->language->get('shipping_methods_title');

        $data['shipping_method_1'] = $this->language->get('shipping_method_1');
        $data['shipping_method_2'] = $this->language->get('shipping_method_2');
        $data['shipping_method_3'] = $this->language->get('shipping_method_3');


        //shipping places
        $data['shipping_places_title'] = $this->language->get('shipping_places_title');

        $data['shipping_place_1'] = $this->language->get('shipping_place_1');

        //payment types
        $data['payment_types_title'] = $this->language->get('payment_types_title');

        $data['payment_types_1'] = $this->language->get('payment_types_1');
        $data['payment_types_2'] = $this->language->get('payment_types_2');


        //breadcrumbs
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/contact')
        );


        $data['column_left']    = $this->load->controller('common/column_left');
        $data['column_right']   = $this->load->controller('common/column_right');
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer']         = $this->load->controller('common/footer');
        $data['header']         = $this->load->controller('common/header');
        
        
        $this->response->setOutput($this->load->view('information/paymentshipping', $data));
    }

}
