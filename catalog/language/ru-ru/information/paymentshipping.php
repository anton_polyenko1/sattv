<?php
// Heading
$_['heading_title']  = 'Оплата и доставка';

// description
$_['details_1']  = 'Заказы, поступившие до 12.00 отправляются в тот же день.';
$_['details_2']  = 'Заказы, поступившие после 12.00 отправляются на следующий день.';
$_['details_3']  = 'Заказы до 200 грн и спутниковые антенны отправляются только по полной предоплате.';
$_['details_4']  = 'В пятницу и воскресенье заказы не отправляются.';

//shipping methods
$_['shipping_methods_title'] = 'Способы доставки';

$_['shipping_method_1'] = 'Новая Почта';
$_['shipping_method_2'] = 'Delivery';
$_['shipping_method_3'] = 'Интайм';

//shipping places
$_['shipping_places_title'] = 'Регионы доставки';

$_['shipping_place_1'] = 'Украина';

//payment types
$_['payment_types_title'] = 'Способы оплаты';

$_['payment_types_1'] = 'Наложенный платеж (при получении у перевозчика)';
$_['payment_types_2'] = 'Банковский платеж';
