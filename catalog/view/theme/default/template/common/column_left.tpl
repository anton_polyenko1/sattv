<?php if ($modules) { ?>
<aside id="column-left" class="col-sm-3 hidden-xs">
    <?php foreach ($modules as $module) { ?>
    <?php echo $module; ?>
    <?php } ?>
    <div>
        <a class="aside-links">О нас</a>      
    </div>
    <div>
        <a class="aside-links" href='https://sattv.com.ua/news/'>Новости</a>      
    </div>
    <div>
        <a class="aside-links" href="https://sattv.com.ua/articles/">Статьи</a>      
    </div>
    <div>
        <a class="aside-links" href="https://sattv.com.ua/comments/">Отзывы</a>      
    </div>
    <div>
        <a class="aside-links" href="https://sattv.com.ua/comments/">Контакты</a> 
        <div id='contacts-left-aside'>
            Sat TV <br>
            Контактное лицо: Ольга <br>
            +380 (95) 083-54-64 <br>
            E-mail: SatTV.com.ua@ukr.net <br>
            Адрес: Украина, Харьковская обл., Харьков, Барабашово на карте <br>
            График работы: Пн, Вт, Ср, Чт, Сб, Вс 8:30 - 14:00 без перерывов; <br>
            Пт выходной 
        </div>
    </div>
    <div>
        <i id="inst-aside-left" class="fa fa-instagram"></i>
        <a target="_blank" rel="noopener" title="" href="https://www.instagram.com/sattv.com.ua/" class="b-link b-link_type_decor b-social-groups__link b-social-groups__link_type_instagram"><i class="b-font-icon b-social-groups__icon b-font-icon__instagram"></i><span class="b-social-groups__title">Instagram</span></a>
    </div>
</aside>
<?php } ?>
