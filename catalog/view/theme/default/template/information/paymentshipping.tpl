<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <ul>
                <li><?php echo $details_1;?></li>
                <li><?php echo $details_2;?></li>
                <li><?php echo $details_3;?></li>
                <li><?php echo $details_4;?></li>
            </ul>

            <h1><?php echo $shipping_methods_title; ?></h1>
            <ul>
                <li><?php echo $shipping_method_1;?></li>
                <li><?php echo $shipping_method_2;?></li>
                <li><?php echo $shipping_method_3;?></li>
            </ul>

            <h1><?php echo $shipping_places_title; ?></h1>
            <ul>
                <li><?php echo $shipping_place_1;?></li>
            </ul>
            <h1><?php echo $payment_types_title; ?></h1>
            <ul>
                <li><?php echo $payment_types_1;?></li>
                <li><?php echo $payment_types_2;?></li>
            </ul>
            <?php echo $column_right; ?></div>
    </div>
    <?php echo $footer; ?>