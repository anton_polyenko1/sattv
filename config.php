<?php
// HTTP
define('HTTP_SERVER', 'http://sattv/');

// HTTPS
define('HTTPS_SERVER', 'http://sattv/');

// DIR
define('DIR_APPLICATION', 'C:/OSPanel/domains/sattv/catalog/');
define('DIR_SYSTEM', 'C:/OSPanel/domains/sattv/system/');
define('DIR_IMAGE', 'C:/OSPanel/domains/sattv/image/');
define('DIR_LANGUAGE', 'C:/OSPanel/domains/sattv/catalog/language/');
define('DIR_TEMPLATE', 'C:/OSPanel/domains/sattv/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OSPanel/domains/sattv/system/config/');
define('DIR_CACHE', 'C:/OSPanel/domains/sattv/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OSPanel/domains/sattv/system/storage/download/');
define('DIR_LOGS', 'C:/OSPanel/domains/sattv/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OSPanel/domains/sattv/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OSPanel/domains/sattv/system/storage/upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'sattv');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
